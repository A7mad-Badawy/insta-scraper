<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get("scrape-user/{user_name}/{pages?}", "InstaScraperController@getUserProfile");
Route::get("scrape-user-bulk", "InstaScraperController@getUserProfilesBulk");


Route::get("scrape-hashtag/{hashtag_name}/{pages?}", "InstaScraperController@getHashtag");
Route::get("scrape-hashtag-bulk", "InstaScraperController@getHashtagsBulk");

Route::get("scrape-location/{location_id}/{pages?}", "InstaScraperController@getLocation");
Route::get("scrape-location-bulk", "InstaScraperController@getLocationsBulk");

Route::get("scrape-post-comments/{short_code}", "InstaScraperController@getPostData");
