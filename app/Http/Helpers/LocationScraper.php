<?php

namespace App\Http\Helpers;

use App\Models\Post;

class LocationScraper
{
    public $scrapeHowManyPages;
    public $after = null;
    public $mediaPosts = [];
    public $mediaPostsRaw = [];
    public $refresh_cache = false;
    public $location_id;

    public function init($location_id, $scrapeHowManyPages, $refresh_cache = false)
    {
        $this->location_id = $location_id;
        $this->refresh_cache = $refresh_cache;
        $this->scrapeHowManyPages = $scrapeHowManyPages;
        $location_data = $this->__getPublicInfo($location_id);
        $location_main_data = $this->__extractPublicData($location_data);
        $location_top_posts = $this->__extractTopPosts($location_data);
        // return $location_top_posts;
        $this->__extractFullPosts($location_id);
        return [
            "other" => [
                "recent_posts_count" => $location_main_data['recent_posts_count'],
                "top_posts_count" => $location_main_data['top_posts_count']
            ],
            "location_data" => $location_main_data,
            "top_posts" => $location_top_posts,
            "recent_posts" => $this->mediaPosts,
            "raw_recent_posts" => $this->mediaPostsRaw,
            "raw_response" => $location_data,
        ];
    }

    private function __getPublicInfo($location_id)
    {
        try {
            $url     = "https://www.instagram.com/explore/locations/$location_id/?__a=1";
            $content = file_get_contents($url);
            $data    = json_decode($content, true);
            return $data;
        } catch (\Exception $e) {
            abort(404, "Location :( $location_id ) wasn't found as an instagram location");
        }
    }

    private function __extractPublicData($data)
    {
        $main_extract_point = $data["graphql"]["location"];
        $location = [];
        $location['id'] = $main_extract_point["id"];
        $location['name'] = $main_extract_point["name"];
        $location['lat'] = $main_extract_point["lat"];
        $location['lng'] = $main_extract_point["lng"];
        $location['slug'] = $main_extract_point["slug"];
        $location['website'] = $main_extract_point["website"];
        $location['phone'] = $main_extract_point["phone"];
        $location['address_json'] = $main_extract_point["address_json"];
        $location['has_public_page'] = $main_extract_point["has_public_page"];
        $location['pic'] = $main_extract_point["profile_pic_url"];
        $location['recent_posts_count'] = $main_extract_point["edge_location_to_media"]["count"];
        $location['top_posts_count'] = $main_extract_point["edge_location_to_top_posts"]["count"];
        $location["directory"] = $main_extract_point["directory"];
        return $location;
    }

    private function __extractTopPosts($data)
    {
        $topPosts = $data["graphql"]["location"]["edge_location_to_top_posts"]["edges"];
        $return = [];

        foreach ($topPosts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("LOCATIONTOPPOSTS", $this->location_id, $this->refresh_cache);
            $return[] = $post_obj->filtered_post_data;
        }
        return $return;
    }



    private function __extractFullPosts($location_id, $after = null)
    {
        if ($this->scrapeHowManyPages <= 0) return;

        $url     = "https://www.instagram.com/explore/locations/217184762/?__a=1&max_id=$after";
        $content = file_get_contents($url);
        $data    = json_decode($content, true);

        $recent_posts =  $data["graphql"]["location"]["edge_location_to_media"]["edges"];
        foreach ($recent_posts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("LOCATION", $this->location_id, $this->refresh_cache);
            if ($already_there && !$this->refresh_cache) {
                $this->cached_mark = true;
                continue;
            } else {
                $this->mediaPosts[] = $post_obj->filtered_post_data;
                $this->mediaPostsRaw[] = $post_obj->raw_obj;
            }
        }

        // $top_posts =  $data["graphql"]["location"]["edge_location_to_top_posts"]["edges"];
        // foreach ($top_posts as $post) {
        //     $current_post = [];
        //     $current_post["id"] = $post["node"]['id'];
        //     $current_post["is_video"] = $post["node"]['is_video'];
        //     $current_post["comments_disabled"] = $post["node"]['comments_disabled'] ?? false;
        //     $current_post["shortcode"] = $post["node"]['shortcode'];
        //     $current_post["post_link"] = "https://www.instagram.com/p/" . $current_post["shortcode"] . "/";
        //     $current_post["dimensions"] = $post["node"]['dimensions'];
        //     $current_post["display_url"] = $post["node"]['display_url'];
        //     $current_post["caption"] = $post["node"]['edge_media_to_caption']["edges"][0]["node"]["text"] ?? null;
        //     $current_post["comments"] = $post["node"]['edge_media_to_comment']["count"];
        //     $current_post["likes"] = $post["node"]['edge_media_preview_like']["count"];
        //     // $current_post["location"] = $post["node"]['location'];
        //     $current_post["accessibility_caption"] = $post["node"]['accessibility_caption'] ?? null;
        //     $current_post["product_type"] = $post["node"]['product_type'] ?? null;
        //     $current_post["video_view_count"] = $post["node"]['video_view_count'] ?? 0;
        //     $current_post["owner"] = $post["node"]['owner']["id"];
        //     $current_post["taken_at_timestamp"] = $post["node"]['taken_at_timestamp'];
        //     $this->top_posts[] = $current_post;
        // }

        if ($this->scrapeHowManyPages > 0 && $data["graphql"]["location"]["edge_location_to_media"]["page_info"]["has_next_page"]) {
            $this->after = $data["graphql"]["location"]["edge_location_to_media"]["page_info"]["end_cursor"];
            $this->scrapeHowManyPages = $this->scrapeHowManyPages - 1;
            $this->__extractFullPosts($location_id, $this->after);
        } else {
            $this->after = null;
            $this->scrapeHowManyPages = 0;
        }
    }
}
