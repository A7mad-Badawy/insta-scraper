<?php

namespace App\Http\Helpers;

use App\Models\Post;

class HashtagScraper
{
    public $scrapeHowManyPages;
    public $after = null;
    public $mediaPosts = [];
    public $mediaPostsRaw = [];
    public $refresh_cache = false;
    public $hashtag_name;

    public function init($hashtag_name, $scrapeHowManyPages, $refresh_cache = false)
    {

        $this->hashtag_name = $hashtag_name;
        $this->refresh_cache = $refresh_cache;
        $this->scrapeHowManyPages = $scrapeHowManyPages;
        $hashtag_data = $this->__getPublicInfo($hashtag_name);
        $hashtag_main_data = $this->__extractPublicData($hashtag_data);
        $hashtag_top_posts = $this->__extractTopPosts($hashtag_data);
        $this->__extractMediaPostsFull($hashtag_name);

        return [
            "other" => [
                "recent_posts_count" => $hashtag_main_data['recent_posts_count'],
                "top_posts_count" => count($hashtag_top_posts)
            ],
            "hashtag_data" => $hashtag_main_data,
            "recent_posts" => $this->mediaPosts,
            "raw_recent_posts" => $this->mediaPostsRaw,
            "top_posts" => $hashtag_top_posts,
            "raw_response" => $hashtag_data
        ];
    }

    private function __getPublicInfo($hashtag_name)
    {
        try {
            $url     = sprintf("https://www.instagram.com/explore/tags/$hashtag_name/");
            $content = file_get_contents($url);
            $content = explode("window._sharedData = ", $content)[1];
            $content = explode(";</script>", $content)[0];
            $data    = json_decode($content, true);
            return $data['entry_data']['TagPage'][0];
        } catch (\Exception $e) {
            abort(404, "Hashtag :( $hashtag_name ) wasn't found as an instagram hashtag");
        }
    }

    private function __extractPublicData($data)
    {
        $main_extract_point = $data["graphql"]["hashtag"];
        $hashtag_data = [];
        $hashtag_data['id'] = $main_extract_point["id"];
        $hashtag_data['name'] = $main_extract_point["name"];
        $hashtag_data['profile_pic_url'] = $main_extract_point["profile_pic_url"];
        $hashtag_data['recent_posts_count'] = $main_extract_point["edge_hashtag_to_media"]["count"];
        $hashtag_data["related_to"] = [];
        foreach ($main_extract_point["edge_hashtag_to_related_tags"]["edges"] as $hashtag) {
            $hashtag_data["related_to"][] = $hashtag["node"]["name"];
        }
        return $hashtag_data;
    }

    private function __extractTopPosts($data)
    {
        $topPosts = $data["graphql"]["hashtag"]["edge_hashtag_to_top_posts"]["edges"];
        $return = [];

        foreach ($topPosts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("HASHTAGTOPPOSTS", $this->hashtag_name, $this->refresh_cache);
            $return[] = $post_obj->filtered_post_data;
        }
        return $return;
    }

    private function __extractMediaPostsFull($hashtag_name, $after = null)
    {
        if ($this->scrapeHowManyPages <= 0) return;
        $url     = "https://www.instagram.com/graphql/query/?query_hash=1780c1b186e2c37de9f7da95ce41bb67&variables={%22tag_name%22:%22$hashtag_name%22,%22first%22:12,%22after%22:%22$after%22}";
        $content = file_get_contents($url);
        $data    = json_decode($content, true);

        $hashtag_posts =  $data["data"]["hashtag"]["edge_hashtag_to_media"]["edges"];
        foreach ($hashtag_posts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("HASHTAG", $this->hashtag_name, $this->refresh_cache);
            if ($already_there && !$this->refresh_cache) {
                $this->cached_mark = true;
                continue;
            } else {
                $this->mediaPosts[] = $post_obj->filtered_post_data;
                $this->mediaPostsRaw[] = $post_obj->raw_obj;
            }
        }

        if ($this->scrapeHowManyPages > 0 && $data["data"]["hashtag"]["edge_hashtag_to_media"]["page_info"]["has_next_page"]) {
            $this->after = $data["data"]["hashtag"]["edge_hashtag_to_media"]["page_info"]["end_cursor"];
            $this->scrapeHowManyPages = $this->scrapeHowManyPages - 1;
            $this->__extractMediaPostsFull($hashtag_name, $this->after);
        } else {
            $this->after = null;
            $this->scrapeHowManyPages = 0;
        }
    }
}
