<?php

namespace App\Http\Helpers;

use App\Models\Post;


class UserScraper
{
    public $scrapeHowManyPages;
    public $after = null;
    public $posts = [];
    public $paginated_posts = [];
    public $cached_mark = false;
    public $refresh_cache = false;
    public $user_name;

    public function init($user_name, $scrapeHowManyPages, $refresh_cache = false)
    {
        $this->user_name = $user_name;
        $this->refresh_cache = $refresh_cache;
        $this->scrapeHowManyPages = $scrapeHowManyPages - 1;
        $user_data = $this->__getPublicInfo($user_name);
        // return $user_data;
        $user_main_data = $this->__extractPublicData($user_data);
        $user_posts = $this->__extractUserPostsFirst12($user_data);
        if (!$this->refresh_cache || !$this->cached_mark) $this->__extractUserPostsFull($user_main_data["id"], $this->after);
        $user_posts = array_merge($user_posts, $this->posts);
        return [
            "other" => ["posts_count" => count($user_posts)],
            "user_data" => $user_main_data,
            "user_posts" => $user_posts,
            "raw_response" => $user_data,
            "paginated_posts" => $this->paginated_posts
        ];
    }

    private function __getPublicInfo($username)
    {
        try {
            $url     = sprintf("https://www.instagram.com/$username");
            $content = file_get_contents($url);
            $content = explode("window._sharedData = ", $content)[1];
            $content = explode(";</script>", $content)[0];
            $data    = json_decode($content, true);
            return $data['entry_data']['ProfilePage'][0];
        } catch (\Exception $e) {
            abort(404, "User Name:( $username ) wasn't found as an instagram user");
        }
    }

    private function __extractPublicData($data)
    {
        $main_extract_point = $data["graphql"]["user"];
        $user = [];
        $user['id'] = $main_extract_point["id"];
        $user['userName'] = $main_extract_point["username"];
        $user['fullName'] = $main_extract_point["full_name"];
        $user['bio'] = $main_extract_point["biography"];
        $user['external_url'] = $main_extract_point["external_url"];
        $user['followed_by'] = $main_extract_point["edge_followed_by"]["count"];
        $user['follow'] = $main_extract_point["edge_follow"]["count"];
        $user['pic'] = $main_extract_point["profile_pic_url_hd"];
        $user['total_posts_count'] = $main_extract_point["edge_owner_to_timeline_media"]["count"];
        return $user;
    }

    private function __extractUserPostsFirst12($data)
    {
        $main_extract_point = $data["graphql"]["user"];
        $return = [];

        if ($main_extract_point["edge_owner_to_timeline_media"]["page_info"]["has_next_page"]) {
            $this->after = $main_extract_point["edge_owner_to_timeline_media"]["page_info"]["end_cursor"];
        } else {
            $this->after = null;
            $this->scrapeHowManyPages = 0;
        }

        $user_posts = $main_extract_point["edge_owner_to_timeline_media"]["edges"];
        foreach ($user_posts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("USER", $this->user_name, $this->refresh_cache);
            if ($already_there && !$this->refresh_cache) {
                $this->cached_mark = true;
                continue;
            } else $return[] = $post_obj->filtered_post_data;
        }
        return $return;
    }

    private function __extractUserPostsFull($user_id, $after)
    {
        if ($this->scrapeHowManyPages <= 0) return;
        $url     = "https://www.instagram.com/graphql/query/?query_hash=472f257a40c653c64c666ce877d59d2b&variables={%22id%22:%22$user_id%22,%22first%22:12,%22after%22:%22$after%22}";
        $content = file_get_contents($url);
        $data    = json_decode($content, true);

        $user_posts =  $data["data"]["user"]["edge_owner_to_timeline_media"]["edges"];
        foreach ($user_posts as $post) {
            $post_obj = new Post();
            $post_obj->filter_post_data($post);
            $already_there = $post_obj->save_post("USER", $this->user_name, $this->refresh_cache);
            if ($already_there && !$this->refresh_cache) {
                $this->cached_mark = true;
                continue;
            } else {
                $this->posts[] = $post_obj->filtered_post_data;
                $this->paginated_posts[] = $post_obj->raw_obj;
            }
        }

        if ($this->scrapeHowManyPages > 0 && $data["data"]["user"]["edge_owner_to_timeline_media"]["page_info"]["has_next_page"]) {
            $this->after = $data["data"]["user"]["edge_owner_to_timeline_media"]["page_info"]["end_cursor"];
            $this->scrapeHowManyPages = $this->scrapeHowManyPages - 1;
            $this->__extractUserPostsFull($user_id, $this->after);
        } else {
            $this->after = null;
            $this->scrapeHowManyPages = 0;
        }
    }
}
