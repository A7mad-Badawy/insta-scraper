<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\UserScraper;
use App\Http\Helpers\HashtagScraper;
use App\Http\Helpers\LocationScraper;
use App\Http\Helpers\PostScraper;
use App\Models\Cached;
use App\Models\Post;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class InstaScraperController extends Controller
{
    public function __construct()
    {
    }

    private function __sendToRabbitMQ($data)
    {
        return;
        $connection = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_PORT'), env('RABBITMQ_USER'), env('RABBITMQ_PASS'));
        $channel = $connection->channel();
        $channel->exchange_declare(env('RABBITMQ_QUEUE'), 'fanout', true, false, false);
        $msg = new AMQPMessage(json_encode($data));
        $channel->basic_publish($msg, env('RABBITMQ_QUEUE'));
        $channel->close();
        $connection->close();
    }

    function getUserProfile(Request $request, $username)
    {
        $pages = ($request->pages == "all")  ? 999 : $request->pages;
        if ($pages == 0 || $pages == null) $pages = 999;
        $refresh_cache = ($request->refresh_cache == "true")  ? true : false;
        $userScraper = new UserScraper();
        $result = $userScraper->init($username, $pages, $refresh_cache);
        return $result;
    }

    function getUserProfilesBulk(Request $request)
    {
        $response = [];
        $users = $request->user;
        foreach ($users as $userName) {
            $response[$userName] = [];
            try {
                $response[$userName]["status"] = "success";
                $response[$userName]["data"] = $this->getUserProfile($request, $userName);
            } catch (\Exception $e) {
                $response[$userName]["status"] = "fail";
                $response[$userName]["data"] = $e->getMessage();
            }
        }
        $this->__sendToRabbitMQ($response);
        return $response;
    }


    function getHashtag(Request $request, $hashtag_name)
    {
        $pages = ($request->pages == "all")  ? 999 : $request->pages;
        if ($pages == 0 || $pages == null) $pages = 999;
        $refresh_cache = ($request->refresh_cache == "true")  ? true : false;
        $hashtagCrawlerObj = new HashtagScraper();
        $result = $hashtagCrawlerObj->init($hashtag_name, $pages, $refresh_cache);
        return $result;
    }

    function getHashtagsBulk(Request $request)
    {
        $response = [];
        $hashtags = $request->hashtag;
        foreach ($hashtags as $hashtag) {
            $response[$hashtag] = [];
            try {
                $response[$hashtag]["status"] = "success";
                $response[$hashtag]["data"] = $this->getHashtag($request, $hashtag);
            } catch (\Exception $e) {
                $response[$hashtag]["status"] = "fail";
                $response[$hashtag]["data"] = $e->getMessage();
            }
        }
        $this->__sendToRabbitMQ($response);
        return $response;
    }



    function getLocation(Request $request, $location_id)
    {
        $pages = ($request->pages == "all")  ? 999 : $request->pages;
        if ($pages == 0 || $pages == null) $pages = 999;
        $refresh_cache = ($request->refresh_cache == "true")  ? true : false;
        $locationCrawlerObj = new LocationScraper();
        $result = $locationCrawlerObj->init($location_id, $pages, $refresh_cache);
        return $result;
    }

    function getLocationsBulk(Request $request)
    {
        $response = [];
        $locations = $request->location;
        foreach ($locations as $location_name => $location) {
            $response[$location_name] = [];
            try {
                $response[$location_name]["status"] = "success";
                $response[$location_name]["data"] = $this->getlocation($request, $location);
            } catch (\Exception $e) {
                $response[$location_name]["status"] = "fail";
                $response[$location_name]["data"] = $e->getMessage();
            }
        }
        return $response;
        $this->__sendToRabbitMQ($response);
    }

    function getPostData($short_code)
    {
        $response = [];
        try {
            $post_obj = Post::where("post_shortcode", $short_code)->latest('id')->first();
            $post_obj->extractPostComments();
            $response["status"] = "success";
            $response["data"] = $post_obj;
        } catch (\Exception $e) {
            $response["status"] = "fail";
            $response["data"] = "Error, " . $e->getMessage();
        }
        return $response;
    }
}
