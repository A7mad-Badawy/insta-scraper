<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $attributes = [];
    protected $post_comments_array = [];


    public function getRawObjAttribute($value)
    {
        return json_decode($value);
    }
    public function getFilteredPostDataAttribute($value)
    {
        return json_decode($value);
    }
    public function getPostCommentsAttribute($value)
    {
        return json_decode($value);
    }
    public function getTaggedUsersAttribute($value)
    {
        return json_decode($value);
    }


    public function filter_post_data($post)
    {
        $node = $post["node"] ?? $post;
        $current_post = [];
        $current_post["id"] = $node['id'];
        $current_post["__typename"] = $node['__typename'] ?? null;
        $current_post["shortcode"] = $node['shortcode'];
        $current_post["post_link"] = "https://www.instagram.com/p/" . $current_post["shortcode"] . "/";
        $current_post["dimensions"] = $node['dimensions'];
        $current_post["display_url"] = $node['display_url'];
        $current_post["display_url"] = $node['display_url'];
        $current_post["caption"] = $node['edge_media_to_caption']["edges"][0]["node"]["text"] ?? null;
        $current_post["accessibility_caption"] = $node['accessibility_caption'] ?? null;

        if (!empty($node['edge_media_to_caption']["edges"])) $current_post["caption"] = $node['edge_media_to_caption']["edges"][0]["node"]["text"];
        $current_post["likes"] = $node['edge_liked_by']["count"] ?? 0;
        $current_post["preview_like"] = $node['edge_media_preview_like']["count"];
        $current_post["location"] = $node['location'] ?? null;
        $current_post["taken_at_timestamp"] = $node['taken_at_timestamp'];
        $current_post["product_type"] = $node['product_type'] ?? null;
        $current_post["video_view_count"] = $node['video_view_count'] ?? 0;
        $current_post["owner"] = $node['owner']["id"];
        $current_post["is_video"] = $node['is_video'];
        $current_post["comments_disabled"] = $node['comments_disabled'] ?? null;
        $current_post["commenting_disabled_for_viewer"] = $node['commenting_disabled_for_viewer'] ?? null;
        $current_post["viewer_can_reshare"] = $node['viewer_can_reshare'] ?? null;
        $current_post["is_ad"] = $node['is_ad'] ?? null;
        $current_post["comments_count"] = $node["edge_media_to_parent_comment"]["count"] ?? null;
        $current_post["has_ranked_comments"] = $node['has_ranked_comments'] ?? null;

        $current_post["post_comments"] = [];
        if (isset($node["edge_media_to_parent_comment"])) foreach ($node["edge_media_to_parent_comment"]["edges"] as $sub) {
            $current_post["post_comments"][] = $sub["node"];
        }
        $current_post["tagged_users"] = [];
        if (isset($node["edge_media_to_tagged_user"])) foreach ($node["edge_media_to_tagged_user"]["edges"] as $sub) {
            $current_post["tagged_users"][] = $sub["node"];
        }

        if ($current_post["__typename"] == "GraphSidecar") {
            $graphSideCar = $node['edge_sidecar_to_children']["edges"] ?? [];
            foreach ($graphSideCar as $sub_post) {
                $sub = [];
                $sub["id"] = $sub_post["node"]['id'];
                $sub["__typename"] = $sub_post["node"]['__typename'];
                $sub["shortcode"] = $sub_post["node"]['shortcode'];
                $sub["post_link"] = "https://www.instagram.com/p/" . $sub["shortcode"] . "/";
                $sub["dimensions"] = $sub_post["node"]['dimensions'];
                $sub["display_url"] = $sub_post["node"]['display_url'];
                $sub["accessibility_caption"] = $sub_post["node"]['accessibility_caption'];
                $sub["is_video"] = $sub_post["node"]['is_video'];
                $current_post["images_content"][] = $sub;
            }
        }

        $this->post_id = $current_post["id"];
        $this->post_shortcode = $current_post["shortcode"];
        $this->raw_obj = json_encode($post);
        $this->filtered_post_data = json_encode($current_post);
        $this->post_comments = json_encode($current_post["post_comments"]);
        $this->tagged_users = json_encode($current_post["tagged_users"]);
    }


    public function save_post($search_type, $search_id, $refresh_cache = false)
    {
        $old_post = Self::firstWhere(["post_id" => $this->post_id, "search_type" => $search_type, "search_id" => $search_id]);
        if ($old_post) {
            if (!$refresh_cache) return "Already There";
            else {
                $old_post->raw_obj = json_encode($this->raw_obj);
                $old_post->filtered_post_data = json_encode($this->filtered_post_data);
                $old_post->post_comments = json_encode($this->post_comments_array);
                $old_post->tagged_users = $this->tagged_users;
                $old_post->updated_at = time();
                $old_post->save();
                return $old_post;
            }
            return false;
        }
        $this->search_type = $search_type;
        $this->search_id = $search_id;
        $this->save();
        return false;
    }

    public function extractPostComments()
    {
        $post_data = $this->__getPostMainObj($this->post_shortcode);
        $this->filter_post_data($post_data["graphql"]["shortcode_media"]);
        $this->__extractFullComments($this->post_shortcode);
        $this->save_post($this->search_type, $this->search_id, $refresh_cache = true);
        return $this;
    }

    private function __getPostMainObj($short_code)
    {
        try {
            $url     = "https://www.instagram.com/p/$short_code/";
            $content = file_get_contents($url);
            $content = explode("window._sharedData = ", $content)[1];
            $content = explode(";</script>", $content)[0];
            $data    = json_decode($content, true);
            return $data['entry_data']['PostPage'][0];
        } catch (\Exception $e) {
            abort(404, "Couldn't find this shortcode: $short_code");
        }
    }

    private function __extractFullComments($short_code, $after = null)
    {

        $client = new \GuzzleHttp\Client();
        $url     = "https://www.instagram.com/graphql/query/?query_hash=33ba35852cb50da46f5b5e889df7d159&variables={\"shortcode\":\"$short_code\",\"first\":50,\"after\":\"$after\"}";
        $res = $client->request('GET', $url);
        if (!$res->getStatusCode() == 200) {
            abort(404, "Something Wrong Happened while retrieving the post comments");
        }
        $data = json_decode($res->getBody(), true);

        $this->post_comments_array[] = $data["data"]["shortcode_media"]["edge_media_to_comment"]["edges"];

        if ($data["data"]["shortcode_media"]["edge_media_to_comment"]["page_info"]["has_next_page"]) {
            $this->after = $data["data"]["shortcode_media"]["edge_media_to_comment"]["page_info"]["end_cursor"];
            $this->__extractFullComments($short_code, $this->after);
        } else {
            $this->after = null;
        }
    }
}
